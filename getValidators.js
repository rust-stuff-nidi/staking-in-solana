const { Connection, clusterApiUrl } = require("@solana/web3.js");

const getValidators = async () => {
  try {
    const connection = new Connection(clusterApiUrl("devnet"), "processed");
    const { current, delinquent } = await connection.getVoteAccounts();

    console.log("All validators: ", current?.concat(delinquent).length);
    console.log("Current validators: ", current?.length);
  } catch (reason) {
    console.error(reason);
  }
};

getValidators();
