const {
  StakeProgram,
  PublicKey,
  sendAndConfirmTransaction,
} = require("@solana/web3.js");
const { createStakeAccount } = require("./createStakeAccount");

const delegateStake = async () => {
  const { connection, wallet, stakeAccount } = await createStakeAccount();
  const validators = await connection.getVoteAccounts();
  const selectedValidator = validators?.current[0];
  const selectedValidatorPubkey = new PublicKey(selectedValidator?.votePubkey);
  const delegateTx = StakeProgram.delegate({
    stakePubkey: stakeAccount.publicKey,
    authorizedPubkey: wallet.publicKey,
    votePubkey: selectedValidatorPubkey,
  });
  const delegateTxId = await sendAndConfirmTransaction(connection, delegateTx, [
    wallet,
  ]);
  console.log(
    `Delegated to ${selectedValidatorPubkey}. Tx Id: ${delegateTxId}`
  );
};

const main = async () => {
  try {
    await delegateStake();
  } catch (reason) {
    console.error(reason);
  }
};

main();
